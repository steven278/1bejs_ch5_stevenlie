const { Profile, User } = require('../models');
const port = process.env.PORT || 3000;
const baseUrl = process.env.BASE_URL || '/api/v1.0';
const { validationResult, check } = require('express-validator');
const crudHelper = require('../helper/crud.helper');

const getAllProfiles = (req, res, next) => {
    const attributes = ['id','user_id','user_country','user_rank','gold_amount'];
    crudHelper.readHelper(req, res, next, Profile, 'Profiles', attributes);
}

const getProfileById = (req, res, next) => {
    crudHelper.readByIdHelper(req, res, next, Profile, 'Profile');
}

const createProfile = async(req, res, next) => {
    try {
        const { user_id, user_country, user_rank, gold_amount } = req.body;
        const profileById = await Profile.findOne({ where : { user_id } });
        if(profileById) {
            throw new Error(`profile with user_id ${user_id} already exist`);
        }
        const obj = { user_id, user_country, user_rank, gold_amount }
        crudHelper.createHelper(req, res, next, Profile, 'Profile', obj);
    } catch (error) {
        next(error);
    }
}

const updateProfile = async(req, res, next) => {
    const { user_country, user_rank, gold_amount } = req.body;
    const obj = { user_country, user_rank, gold_amount };
    crudHelper.updateHelper(req, res, next, Profile, 'Profile', obj);
}

const deleteProfile = (req, res, next) => {
    crudHelper.deleteHelper(req, res,next, 'Profile');
}

module.exports = {
    getAllProfiles,
    getProfileById,
    createProfile,
    updateProfile,
    deleteProfile
}

