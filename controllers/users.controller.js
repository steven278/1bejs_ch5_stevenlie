const { User, Profile, History } = require('../models');
const port = process.env.PORT || 3000;
const baseUrl = process.env.BASE_URL || '/api/v1.0';
const { validationRes, nextult, check } = require('express-validator');
const crudHelper = require('../helper/crud.helper');

const getAllUsers = (req, res, next) => {
    const attributes = ['id', 'username', 'password', 'email'];
    crudHelper.readHelper(req, res, next, User, 'Users', attributes);
}

const getUserById = (req, res, next) => {
    crudHelper.readByIdHelper(req, res, next, User, 'User');
}

const createUser = (req, res, next) => {
    const { username, password, email } = req.body;
    const obj = { username, password, email };
    crudHelper.createHelper(req, res, next, User, 'User', obj);
}

const updateUser = (req, res, next) => {
    const { username, password, email } = req.body;
    const obj = { username, password, email }
    crudHelper.updateHelper(req, res, next, User, 'User', obj);
}

const deleteUser = (req, res, next) => {
    crudHelper.deleteHelper(req, res, next, 'User');
}

module.exports = {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
}

