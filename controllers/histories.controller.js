const { History } = require('../models');
const port = process.env.PORT || 3000;
const baseUrl = process.env.BASE_URL || '/api/v1.0';
const { validationResult, check } = require('express-validator');
const crudHelper = require('../helper/crud.helper');

const getAllHistories = async(req, res, next) => {
    const attributes = ['id', 'user_id', 'match_duration_in_minutes', 'match_score'];
    crudHelper.readHelper(req, res, next, History, 'Histories', attributes);
}

const getHistoryById = async(req, res, next) => {
    crudHelper.readByIdHelper(req, res, next, History, 'History');
}

const createHistory= async(req, res, next) => {
    const { user_id, match_duration_in_minutes, match_score } = req.body;
    const obj = { user_id, match_duration_in_minutes, match_score }
    crudHelper.createHelper(req, res, next, History, 'History', obj);
}

const updateHistory = async(req, res, next) => {
    const { match_duration_in_minutes, match_score } = req.body;
    const obj = { match_duration_in_minutes, match_score };
    crudHelper.updateHelper(req, res, next, History, 'History', obj);
}

const deleteHistory = async(req, res, next) => {
    crudHelper.deleteHelper(req, res, next, 'History');
}

module.exports = {
    getAllHistories,
    getHistoryById,
    createHistory,
    updateHistory,
    deleteHistory
}

