require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const baseUrl = process.env.BASE_URL || '/api/v1.0';
const { router } = require('./routes/routes');
const methodOverride = require('method-override');
const morgan = require('morgan');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./api-documentation/swagger.json')

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(express.json());
app.use(express.urlencoded( { extended : true } ))
app.use(methodOverride('_method'));
app.set('view engine', 'ejs');

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

app.use(router);

app.listen(port, () => {
    console.log(`App is listening on port ${port}`);
});

