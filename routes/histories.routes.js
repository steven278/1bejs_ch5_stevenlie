const express = require('express');
const router = express.Router();
const { historyValidation, validate } = require('../validation/validator');
const { getAllHistories, getHistoryById, createHistory, updateHistory, deleteHistory } = require('../controllers/histories.controller');

router.get('/', getAllHistories);
router.get('/:id', getHistoryById);
router.post('/', historyValidation('create'), validate, createHistory);
router.put('/:id', historyValidation('update'), validate, updateHistory);
router.delete('/:id', deleteHistory);

module.exports = router;