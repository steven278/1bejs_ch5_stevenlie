const express = require('express');
const router = express.Router();
const { userValidation, validate } = require('../validation/validator');
const { getAllUsers, getUserById, createUser, updateUser, deleteUser } = require('../controllers/users.controller');

router.get('/', getAllUsers);
router.get('/:id', getUserById);
router.post('/', userValidation(), validate, createUser);
router.put('/:id', userValidation(), validate, updateUser);
router.delete('/:id', deleteUser);

module.exports = router;