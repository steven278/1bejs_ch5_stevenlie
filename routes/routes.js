const express = require('express');
const router = express.Router();
const userRoutes = require('./users.routes');
const profileRoutes = require('./profiles.routes');
const historyRoutes = require('./histories.routes');
const { Admin } = require('../models');


const admin = {
    email : 'steve@mail.com',
    password : '123456'
}

const checkDbAdmin = async (req, res, next) => {
    const authenticatedAdmin =  await Admin.findAll();
    const { email, password } = authenticatedAdmin[0];
    if(admin.email == email && admin.password == password){
        next();
    }else{
        res.status(401).json({
            status: 'unauthorized',
            message : 'invalid email or password'
        })
    }
}

router.use(checkDbAdmin)

router.get('/', (req, res) => {
    res.status(200).json({
        status : 'success',
        message : 'hello world'
    })
})

router.use('/users', userRoutes);
router.use('/profiles', profileRoutes);
router.use('/histories', historyRoutes);

router.use((err, req, res, next) => {
    if(err.name === 'SequelizeDatabaseError' || err.name === 'SequelizeUniqueConstraintError' ||  err.name === 'ReferenceError' || err.name === 'SequelizeForeignKeyConstraintError'){
        res.status(400).json({
            status : 'bad request',
            errorName : err.name,
            message : err.message
        });
    }else if( err.name === 'Error' ){
        res.status(404).json({
            status : 'not found',
            errorName : err.name,
            message : err.message
        });
    }
    res.status(500).json({
        status : 'internal server error',
        errorName : err.name,
        message : err.message
    });
})



module.exports = { router }