const express = require('express');
const router = express.Router();
const { profileValidation, validate } = require('../validation/validator');
const { getAllProfiles, getProfileById, createProfile, updateProfile, deleteProfile } = require('../controllers/profiles.controller');

router.get('/', getAllProfiles);
router.get('/:id', getProfileById);
router.post('/', profileValidation('create'), validate, createProfile);
router.put('/:id', profileValidation('update'), validate, updateProfile);
router.delete('/:id', deleteProfile);

module.exports = router;