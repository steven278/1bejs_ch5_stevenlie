'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      // define association here
      User.hasOne(models.Profile, {
        foreignKey : 'user_id',
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      });
      User.hasMany(models.History, {
        foreignKey : 'user_id',
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      });
    }
  }
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};