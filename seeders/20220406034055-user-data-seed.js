'use strict';

const userData = require('../masterdata/users.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const userDataTimestamped = userData.map((e)=> {
      e['createdAt'] = new Date();
      e['updatedAt'] = new Date();
      return e
    })
    await queryInterface.bulkInsert('Users', userDataTimestamped)
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
